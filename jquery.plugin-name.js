/**
 * jQuery plugin boilerplate 
 * @author Thierry Langlois
 * 
 * @example
 * <div data-plugin-name='{ "foo": "valuedViaElement" }'></div>
 * <script>$(selector).pluginName({ foo: 'valuedViaInstance' });</script>
 * 
 * /!\ While using attribute options syntax, uppercases are converted to lowercases prefixed with dash (-)
 * cf https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/dataset
 */

/**
 * TODO
 * - event subscription
 * - collection method call (not only via instance)
 */

/**
 * /!\ Make sure to use an IIFE. It guarantee that $ is jQuery inside your function.
 * The reason is that $ sign can be overrided. Others libraries may also use $ as a shortcut or can be setted to undefined.
 */
(function ($) {

  /**
   * Name of the plugin
   * @type {String}
   */
  var pluginName = 'myPlugin';

  /**
   * Data attribute suffix
   * @type {String}
   */
  var dataKey = 'plugin_' + pluginName;

  /**
   * Cache all instances
   * @type {Array}
   */
  var instances = [];

  /**
   * Static methods
   * @type {Object}
   */
  var staticMethods = {
    triggerResizeEvent: function () {
      return $.each(instances, function(){
        this.onResize();
      });
    }
  };

  /**
   * Global events
   */
  $(window).on('resize.' + pluginName, staticMethods.triggerResizeEvent);

  /**
   * Add a static method to jQuery object
   * In this case, we can :
   * - access to a specific instance via $.myPlugin('elementSelector');
   * - trigger some static methods
   */
  $[pluginName] = function (param) {

    // Params handling
    if (!param) {
      return;
    }

    // Call static method
    if (typeof param === 'string' && staticMethods[param]) {
      return staticMethods[param]();
    }

    // Instance getter
    else {
      param = $(param);
      if (param.length && param.data(dataKey)) {
        return param.data(dataKey);
      }
    }

  };

  /**
   * jQuery classical plugin instanciation loop using plugin name
   * Create a new public method in jQuery object prototype
   * prevent multiple instantiations
   * @param {Object} instantiation options
   */
  $.fn[pluginName] = function (options) {
    return this.each(function () {
      var instance;
      if (typeof options === 'string') {
        instance = $[pluginName](this);
        if (instance && instance[options]) {
          instance[options]();
        }
      }
      else if (typeof options === 'object' && !$.data(this, dataKey)) {
        instance = new Plugin(this, options);
        $.data(this, dataKey, instance);
        instances.push(instance);
      }
    });
  };

  /**
   * Plugin constructor
   * @class
   * @param {HTMLElement} HTML element from the jQuery instanciation loop 
   * @param {Object} instantiation options
   */
  function Plugin (element, options) {

    // Convert main element to jQuery element
    this.$element = $(element);

    // Merge element options w/ instanciation options w/ default options
    this.config = $.extend({},
      Plugin.defaults,
      options,
      this.$element.data(pluginName)
    );
    
    // Reach DOM elements needed
    this.$fooBar = $('.fooBar', this.$element);
    
    // Initialization process
    if (this.config.autoInit) {
      this.init();
    }
  };

  /**
   * Plugin default options
   * @default
   * @type {Object}
   */
  Plugin.defaults = {
    autoInit: true,
    foo: 'default',
    customCallback: function(){}
  };

  /**
   * Alias for Plugin object prototype (jQuery like)
   */
  Plugin.fn = Plugin.prototype;
  
  /**
   * Initialization logic
   */
  Plugin.fn.init = function () {
    this.config.customCallback.apply(this);
  };

  /**
   * Destroy
   */
  Plugin.fn.destroy = function () {
    // Remove attribute data
    this.$element.removeData(dataKey);
    
    // remove subsriber
    removeEventSubscriber(this);
    
    // Set instance var to null
    return null;
  };
  
  /**
   * Public method sample
   */
  Plugin.fn.publicMethod = function (foo) {
    var bar = privateMethod.apply(this, [foo]);
  };
  
  /**
   * on resize
   */
  Plugin.fn.onResize = function () {
    console.debug('onResize');
  };

  /**
   * Private method sample
   * If instanciation context is needed
   * call it via ‘Function.prototyp.apply‘ method
   * cf ‘publicMethod‘ sample
   */
  function privateMethod (str) {
    return str.toUpperCase() + ' ' + this.config.foo;
  }

  /**
   * Remove an instance from global events subscibers list
   * @param  {[type]} instance — Plugin instance
   */
  function removeEventSubscriber (instance) {
    var indexToRemove = -1;
    if (instances && instances.length) {
      for (var i = 0, l = instances.length; i < l; i++) {
        if (instances[i] === instance) {
          indexToRemove = i;
          break;
        }
      }
    }
    if (indexToRemove !== -1) {
      instances.splice(indexToRemove, 1);
    }
  }

}(jQuery));